---
layout: handbook-page-toc
title: "Account Based Strategy"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Account Based Marketing
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/](/handbook/marketing/account-based-marketing/)

## Ideal Customer Profile
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/ideal-customer-profile/](/handbook/marketing/account-based-marketing/ideal-customer-profile/)

## Focus account lists
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/focus-account-list/](/handbook/marketing/account-based-marketing/focus-account-list/)

## Demandbase
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/#project-management](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/)

## How we work 

### Project and priority management
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/#project-management](/handbook/marketing/account-based-marketing/#project-management)

### Projects
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/#project-management](/handbook/marketing/account-based-marketing/#project-management)

### Labels
Please reference the updated page here (and bookmark for future reference): [/handbook/marketing/account-based-marketing/#project-management](/handbook/marketing/account-based-marketing/#project-management)
